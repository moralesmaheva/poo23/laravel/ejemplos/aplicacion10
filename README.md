# OBJETIVO

Realizar una aplicación completa con Backend y Frontend

Vamos a utilizar para realizar la parte del CRUD un paquete backpack

# SISTEMA DE INFORMACION

La tabla productos :

-   nombre
-   precio

La tabla ventas:

-   producto
-   vendedor
-   cantidad

La tabla vendedores:

-   nombre
-   apellidos
-   correo

# 1 PASO

Realizar los modelos y migraciones para cada tabla

```php
php artisan make:model Producto -m
php artisan make:model Vendedor -m
php artisan make:model Venta -m
```

Ahora coloco en los modelos los campos de asignacion masiva

```php
protected $fillable = [
        'nombre',
        'apellidos',
        'email',
    ];
```

Ahora creo los campos en las migraciones

```php
public function up(): void
    {
        Schema::create('vendedors', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('email')->unique();
            $table->timestamps();
        });
    }
```

Creo las relaciones (claves ajenas)

```php
public function up(): void
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad');
            //clave ajena a la tabla productos
            $table->unsignedBigInteger('producto_id');
            $table
                ->foreign('producto_id')
                ->references('id')
                ->on('productos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            //clave ajena a la tabla vendedores
            $table->unsignedBigInteger('vendedor_id');
            $table
                ->foreign('vendedor_id')
                ->references('id')
                ->on('vendedores')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }
```

Ejecuto las migraciones

```php
php artisan migrate
```
